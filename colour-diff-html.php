<?php
    $GLOBALS['data'] = '';

    while ($input = fread(STDIN, 1024)) {
        $GLOBALS['data'] .= $input;
    }

    convertControlCodes();
    echo $GLOBALS['data'];

    function convertControlCodes() {
        $ControlCodes = array(
            "\e[m"   => '</span>',
            "\e[1m"  => '<span class="diffdim">',
            "\e[36m" => '<span class="diffdim">',
            "\e[32m" => '<span class="diffadd">',
            "\e[31m" => '<span class="diffdel">',
        );

        foreach ($ControlCodes as $k => $v) {
            $GLOBALS['data'] = str_replace($k, $v, $GLOBALS['data']);
        }
    }
?>
